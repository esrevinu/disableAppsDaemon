TARGET := disable-apps-daemon
NDK := /opt/android-sdk/ndk-bundle
CC := $(NDK)/toolchains/llvm/prebuilt/linux-x86_64/bin/clang
ABI := aarch64-linux-android24
LIBS := -llog

all : $(TARGET)

$(TARGET) : disable-apps-daemon.c
	$(CC) -target $(ABI) -o $@ $^ $(LIBS)

