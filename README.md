# 빌드 방법

## NDK 사용

`with_ndk.mk`를 적절히 고친 다음 다음을 실행하면 실행파일이 생성된다.

```
make -f with_ndk.mk
```

## Lineage OS 소스 사용

이 디렉토리를 Lineage OS 소스 디렉토리 `system/` 아래에 넣고 크를

```
$ cd <Lineage OS 소스 디렉토리>
$ source build/envsetup.sh
$ breakfast <장치 이름>  # Xperia Z5 Compact는 suzuran
$ cd system/disableAppsDaemon
$ mma
```

실행시키면 `out/target/.../system/bin/` 아래에 실행파일이 생성된다.

# SELinux 설정 변경

장치의 부트 이미지에는 SELinux file contexts나 SE 정책 파일이 들어 있다. 이
파일도 이 데몬이 일을 할 수 있도록 변경되어야 한다. 이것도 Lineage OS 소스
디렉토리 아래 `system/sepolicy/`의 파일을 고치고 컴파일해서 얻을 수 있다.
`sepolicy.patch` 파일이 그 패치파일이다. 여기서도 `mma`를 실행시키면 여러 파일이
`out/target/.../root/` 디렉토리 아래에 생기는데 `file_contexts.bin`과 `sepolicy`
파일만 필요하다.

# 장치에 데몬 실행 파일 넣기

리커버리로 들어가서 기기의 `/system/bin`에 데몬 실행 파일을 복사하고 SELinux
file context 변경해 준다.

```
$ adb push disable-apps-daemon /system/bin
$ adb shell chcon u:object_r:mydad_exec:s0 /system/bin/disable-apps-daemon
```

# 부트 이미지 변경하기

이 작업은 부트 이미지를 풀어서 안의 램디스크를 수정하고 다시 묶는 작업인데 이의
목적은 `init`에 데몬을 서비스로 등록하기 위함이다. 패치에 충전 임계점 설정하는
부분과 adb에 루트 권한을 줄 수 없게 `ro.debuggable=0`으로 변경하는 부분이 있는데
데몬과는 관계없으므로 반드시 필요한 것은 아니다.

Lineage OS 소스 `system/core/mkbootimg`에 부트 이미지를 풀고 묶는 도구가
존재한다. `unpackbootimg`로 풀면 여러 값과 함께 `zImage`와 압축된 cpio
램디스크가 나오는데 이 램디스크를 풀어서 위에서 만든 `file_contexts.bin`과
`sepolicy`를 넣고 `ramdisk_llk_mydad_root.patch`로 패치시켜 준다.

다시 램디스크를 묶고 `mkbootimg`로 풀 때 나왔던 여러 파라미터 값을 가지고
부트이미지를 만든다.

