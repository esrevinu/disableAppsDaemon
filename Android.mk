LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := disable-apps-daemon
LOCAL_CFLAGS := -Wall -Werror
LOCAL_SHARED_LIBRARIES := \
        liblog
LOCAL_SRC_FILES := \
        disable-apps-daemon.c

include $(BUILD_EXECUTABLE)

