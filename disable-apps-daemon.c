#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <android/log.h>

#define LOGTAG "disable-apps"

#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOGTAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , LOGTAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO   , LOGTAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN   , LOGTAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , LOGTAG, __VA_ARGS__)

#define ENABLE_LLK "/sys/class/power_supply/battery/enable_llk"
#define LLK_SOCMAX "/sys/class/power_supply/battery/llk_socmax"
#define LLK_SOCMIN "/sys/class/power_supply/battery/llk_socmin"

void disable_enable_apps(int disable, char **linep, size_t *lenp, FILE *fp);
void adjust_charge_limit(char **linep, size_t *lenp, FILE *fp);
void request_charge_limit(FILE *fp);
void write_llk(const char *path, int val, FILE *fp);
void read_llk(const char *path, int *val, FILE *fp);
void get_da_uid();
int check_peer_cred(int c_sockfd);

int da_uid = 0;
const char *DA_APP = "org.duckdns.hunmaul.disableapps";

int main()
{
  int s_sockfd, c_sockfd;
  struct sockaddr_un saddr, caddr;
  int pid, res;
  socklen_t clen;
  /* Linux Abstract Socket Namespace */
  const char name[] = "\0disable_apps_socket";
  const int struct_size = sizeof(saddr.sun_family) + sizeof(name) - 1;

  LOGI("Start disable-apps-daemon");

  memset(&saddr, 0x0, sizeof(struct sockaddr_un));
  saddr.sun_family = AF_UNIX;
  if (*name == '\0') {
    memcpy(saddr.sun_path, name, sizeof(name) - 1);
  } else {
    strncpy(saddr.sun_path, name, sizeof(saddr.sun_path) - 1);
    unlink(name);
  }

  if ((s_sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
    LOGE("Cannot create socket");
    return -1;
  }

  if ((res = bind(s_sockfd, (struct sockaddr *)&saddr, struct_size)) < 0) {
    LOGE("Cannot bind socket");
    return res;
  }

  if ((res = listen(s_sockfd, 5)) < 0) {
    LOGE("listen error");
    return res;
  }

  LOGD("Server waiting client...");
  while (1) {
    FILE *fp;
    c_sockfd = accept(s_sockfd, (struct sockaddr *)&caddr, &clen);
    pid = fork();
    if (pid == 0) {
      char *line = NULL;
      size_t len = 0;
      int check = 0;

      LOGD("New client connected...");
      if (c_sockfd == -1) {
        LOGE("Accept error");
        exit(1);
      }

      check = check_peer_cred(c_sockfd);

      if ((fp = fdopen(c_sockfd, "r+")) == NULL) {
        LOGE("fdopen error");
        exit(1);
      }
      if ((res = getline(&line, &len, fp)) != -1) {
        if (res > 0 && line[res - 1] == '\n') {
          line[res - 1] = '\0';
          res--;
        }
        if (check && strcmp(line, "disable-apps") == 0)
          disable_enable_apps(1, &line, &len, fp);
        else if (check && strcmp(line, "enable-apps") == 0)
          disable_enable_apps(0, &line, &len, fp);
        else if (check && strcmp(line, "adjust-charge-limit") == 0)
          adjust_charge_limit(&line, &len, fp);
        else if (strcmp(line, "request-charge-limit") == 0)
          request_charge_limit(fp);
        else {
          if (check) {
            LOGW("Unknown action: action = %s", line);
            if (fprintf(fp, "result: unknown action: %s\n", line) < 0) {
              LOGE("Output error to client socket");
            }
          } else {
            LOGW("No credential: action = %s", line);
            if (fprintf(fp, "result: no credential: %s\n", line) < 0) {
              LOGE("Output error to client socket");
            }
          }
        }
      }

      free(line);
      fclose(fp);
      exit(0);
    }
    close(c_sockfd);
  }

  return 0;
}

void get_da_uid()
{
  char *cmd = NULL;
  FILE *fp = NULL;

  if (asprintf(&cmd, "/system/bin/dumpsys package %s | grep userId", DA_APP) == -1) {
    LOGE("cmd allocation failed");
    goto error;
  }

  fp = popen(cmd, "r");
  if (fp == NULL) {
    LOGE("popen error");
    goto error;
  }

  if (fscanf(fp, " userId=%d", &da_uid) != 1) {
    LOGE("error in getting userId from command output");
    goto error;
  }

  LOGD("get_da_uid: %d", da_uid);
  free(cmd);
  pclose(fp);
  return;

 error:
  da_uid = -1;
  if (cmd) free(cmd);
  if (fp) pclose(fp);
}

int check_peer_cred(int c_sockfd)
{
  struct ucred cred;
  socklen_t len = sizeof(cred);
  uid_t uid;

  if (getsockopt(c_sockfd, SOL_SOCKET, SO_PEERCRED, &cred, &len) < 0) {
    LOGE("getsockopt error");
    return 0;
  }

  uid = cred.uid;

  LOGD("peer uid = %d", uid);

  if (da_uid == 0) get_da_uid();

  if (da_uid < 0) {
    LOGW("Not found a needed package: %s", DA_APP);
    return 0;
  }

  return da_uid == uid ? 1 : 0;
}

void disable_enable_apps(int disable, char **linep, size_t *lenp, FILE *fp)
{
  int res;
  char *app;
  char *cmd = NULL;
  char *str_able;
  int count = 0;

  if (disable) {
    str_able = "disable";
  } else {
    str_able = "enable";
  }

  while ((res = getline(linep, lenp, fp)) != -1) {
    app = *linep;
    if (res > 0 && app[res - 1] == '\n') {
        app[res - 1] = '\0';
        res--;
    }
    if (res > 1) {
      LOGI("%s_apps: %s", str_able, app);
      if (asprintf(&cmd, "/system/bin/pm %s %s", str_able, app) == -1) {
        LOGE("cmd allocation failed");
        if (fprintf(fp, "result: cmd allocation failed\n") < 0) {
          LOGE("Output error to client socket");
        }
        exit(1);
      }
      res = system(cmd);
      if (res == 0) count++;
      free(cmd);
    } else if (res == 1 && app[0] == '.') {
      break;
    } else {
      LOGW("%s_apps: too short app name", str_able);
    }
  }
  if (fprintf(fp, "result: %d apps %sd\n", count, str_able) < 0) {
    LOGE("Output error to client socket");
  }
}

void adjust_charge_limit(char **linep, size_t *lenp, FILE *fp)
{
  int llk, max, min;
  int res;

  if ((res = getline(linep, lenp, fp)) == -1) {
    LOGE("No charge limit values");
    if (fprintf(fp, "result: No charge limit values\n") < 0) {
      LOGE("Output error to client socket");
    }
    exit(1);
  }

  res = sscanf(*linep, "%d %d %d", &llk, &max, &min);
  if (res != 3) {
    LOGE("Need three integer values: count = %d", res);
    if (fprintf(fp, "result: Need three integer values: count = %d\n",
                res) < 0) {
      LOGE("Output error to client socket");
    }
    exit(1);
  }
  LOGI("adjust_charge_limit: %d %d %d", llk, max, min);

  if (llk == 0 || llk == 1) {
    write_llk(ENABLE_LLK, llk, fp);
  } else {
    LOGE("enable_llk should be 0 or 1: %d", llk);
    if (fprintf(fp, "result: enable_llk should be 0 or 1: %d\n", llk) < 0) {
      LOGE("Output error to client socket");
    }
    exit(1);
  }

  if (llk == 1 && (max < min || max < 50 || min < 30 || (max - min) < 5)) {
    LOGW("Not adequate max min values: %d %d", max, min);
    LOGW("Set to 85 50");
    max = 85;
    min = 50;
  }
  if (llk == 0) {
    max = min = 0;
  }

  write_llk(LLK_SOCMAX, max, fp);
  write_llk(LLK_SOCMIN, min, fp);

  if (fprintf(fp, "result: success\n") < 0) {
    LOGE("Output error to client socket");
  }
}

void write_llk(const char *path, int val, FILE *fp)
{
  FILE *sysfp = NULL;

  if ((sysfp = fopen(path, "w")) == NULL) {
    LOGE("File open error: %s", path);
    if (fprintf(fp, "result: file open error: %s\n", path) < 0) {
      LOGE("Output error to client socket");
    }
    exit(1);
  }
  if (fprintf(sysfp, "%d\n", val) < 0) {
    LOGE("File write error: %s", path);
    if (fprintf(fp, "result: file write error: %s\n", path) < 0) {
      LOGE("Output error to client socket");
    }
    exit(1);
  }
  fclose(sysfp);
}

void request_charge_limit(FILE *fp)
{
  int llk, max, min;

  read_llk(ENABLE_LLK, &llk, fp);
  read_llk(LLK_SOCMAX, &max, fp);
  read_llk(LLK_SOCMIN, &min, fp);

  LOGI("request_charge_limit: llk=%d max=%d min=%d", llk, max, min);

  if (fprintf(fp, "charge-limit %d %d %d\n", llk, max, min) < 0) {
    LOGE("Output error to client socket");
  }
}

void read_llk(const char *path, int *val, FILE *fp)
{
  FILE *sysfp = NULL;

  if ((sysfp = fopen(path, "r")) == NULL) {
    LOGE("File open error: %s", path);
    if (fprintf(fp, "result: file open error: %s\n", path) < 0) {
      LOGE("Output error to client socket");
    }
    exit(1);
  }
  if (fscanf(sysfp, "%d", val) == EOF) {
    LOGE("File read error: %s", path);
    if (fprintf(fp, "result: file write error: %s\n", path) < 0) {
      LOGE("Output error to client socket");
    }
    exit(1);
  }
  fclose(sysfp);
}
